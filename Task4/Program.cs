﻿using System.Xml.Linq;

internal class Program
{
    struct Person
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string FlatNumber { get; set; }
        public string MobilePhone { get; set; }
        public string FlatPhone { get; set; }

        public void saveAsXml(string path)
        {
            XElement person = new XElement("Person");
            XAttribute personName = new XAttribute("name", Name);
            person.Add(personName);

            XElement address = new XElement("Address");
            XElement street = new XElement("Street", Street);
            XElement houseNumber = new XElement("HouseNumber", HouseNumber);
            XElement flatNumber = new XElement("FlatNumber", FlatNumber);
            address.Add(street);
            address.Add(houseNumber);
            address.Add(flatNumber);
            person.Add(address);

            XElement phones = new XElement("Phones");
            XElement mobilePhone = new XElement("MobilePhone", MobilePhone);
            XElement flatPhone = new XElement("FlatPhone", FlatPhone);
            phones.Add(mobilePhone);
            phones.Add(flatPhone);
            person.Add(phones);

            person.Save(path);
        }
    }

    private static void Main(string[] args)
    {       
        Person p = new Person();

        Console.WriteLine("Input information about person");

        Console.WriteLine("Name:");
        p.Name = Console.ReadLine();
        Console.WriteLine("Street:");
        p.Street = Console.ReadLine();
        Console.WriteLine("House number:");
        p.HouseNumber = Console.ReadLine();
        Console.WriteLine("Flat number:");
        p.FlatNumber = Console.ReadLine();
        Console.WriteLine("Mobile phone:");
        p.MobilePhone = Console.ReadLine();
        Console.WriteLine("Flat phone:");
        p.FlatPhone = Console.ReadLine();

        p.saveAsXml("person.xml");
    }
}