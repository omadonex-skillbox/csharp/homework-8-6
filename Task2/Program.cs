﻿using System.Globalization;

internal class Program
{
    private static void Main(string[] args)
    {
        Dictionary<string, string> dict = fillDictionary();
        findOwner(dict);        
    }

    private static Dictionary<string, string> fillDictionary()
    {
        Console.WriteLine("Filling dictionary");
        Dictionary<string, string> dict = new Dictionary<string, string>();
        string phone;
        do
        {
            Console.WriteLine("Input a phone number");
            phone = Console.ReadLine();
            if (phone != "")
            {
                Console.WriteLine("Input an owner");
                dict.Add(phone, Console.ReadLine());
            }
        } while (phone != "");

        return dict;
    }

    private static void findOwner(Dictionary<string, string> dict)
    {
        Console.WriteLine("SEARCHING: Input a phone number for search");
        string phone = Console.ReadLine();
        string name;
        dict.TryGetValue(phone, out name);
        if (name != null)
        {
            Console.WriteLine(name);
        }
        else
        {
            Console.WriteLine("Phone not found");
        }
    }
}