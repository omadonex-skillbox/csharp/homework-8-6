﻿internal class Program
{
    private static void Main(string[] args)
    {
        List<int> list = CreateList();
        PrintList(list);
        list.RemoveAll(item => item > 25 && item < 50);
        PrintList(list);
    }

    private static List<int> CreateList()
    {
        Random rnd = new Random();
        List<int> list = new List<int>();
        for (int i = 0; i < 100; i++)
        {
            list.Add(rnd.Next(100));
        }

        return list;
    }

    private static void PrintList(List<int> list)
    {
        foreach (int item in list)
        {
            Console.Write(item.ToString() + " ");
        }
        Console.WriteLine();
    }
}