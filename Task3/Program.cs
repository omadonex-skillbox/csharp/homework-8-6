﻿internal class Program
{
    private static void Main(string[] args)
    {
        string input;

        HashSet<int> set = new HashSet<int>();
        do
        {
            Console.WriteLine("Input a number ot type 'exit' to exit");
            input = Console.ReadLine();
            int number;
            bool isNum = int.TryParse(input, out number);
            if (!isNum)
            {
                if (input != "exit")
                {
                    Console.WriteLine("Input only numbers");
                }
            } else
            {                
                if (set.Contains(number))
                {
                    Console.WriteLine("Number already presented in a set");
                } else
                {
                    set.Add(number);
                    Console.WriteLine("Number successfully added to a set");
                }
            }
        } while (input != "exit");
    }
}